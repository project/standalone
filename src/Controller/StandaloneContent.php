<?php

namespace Drupal\standalone\Controller;

use Drupal\Core\Ajax\AjaxHelperTrait;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Page controller class for standalone content.
 */
class StandaloneContent extends ControllerBase implements ContainerAwareInterface {

  use ContainerAwareTrait;
  use AjaxHelperTrait;

  /**
   * The Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * StandaloneContent constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   Renderer service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, RendererInterface $renderer) {
    $this->entityTypeManager = $entity_type_manager;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('renderer')
    );
  }

  /**
   * Callback for standalone page.
   *
   * @param string $type
   *   The entity type to render.
   * @param string $id
   *   The entity ID to render.
   * @param string $extra
   *   Additional options.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The markup for the page.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function page($type, $id, $extra = NULL) {
    // Check that the entity exists.
    $entity = $this->loadEntity($type, $id);
    if (empty($entity)) {
      throw new NotFoundHttpException();
    }

    // For views, access checks use the display ID.
    if ($type == 'view') {
      $operation = 'default';
      if ($extra) {
        $operation = $extra;
      }
      $access = $entity->getExecutable()->access($operation);
    }
    else {
      // Check access to view the entity.
      $access = $entity->access('view');
    }

    if (!$access) {
      throw new AccessDeniedHttpException();
    }

    // Prepare the render array.
    $build = $this->viewEntity($entity, $type, $extra);

    // Allow other modules to alter the output before rendering.
    $this->moduleHandler()->invokeAll('standalone_build_alter', [
      &$build,
      $entity,
      $extra,
    ]);

    if ($this->isAjax()) {
      return $build;
    }

    $markup = $this->renderer->render($build);

    // Allow other modules to alter the output after rendering.
    $this->moduleHandler()->invokeAll('standalone_rendered_alter', [
      &$markup,
      $entity,
      $extra,
    ]);

    return new Response('<html><body>' . $markup . '</body></html>');
  }

  /**
   * Load the relevant entity.
   *
   * @param string $type
   *   The entity type to render.
   * @param string $id
   *   The entity ID to render.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The entity, if it exists.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function loadEntity($type, $id) {
    // Try loading the entity.
    $storage = $this->entityTypeManager->getStorage($type);
    $entity = $storage->load($id);
    if ($entity) {
      // We have the entity, so return it.
      return $entity;
    }

    return NULL;
  }

  /**
   * Get the view mode to use.
   *
   * @param string $type
   *   The entity type.
   *
   * @return string
   *   The view mode to use for rendering.
   */
  private function getViewMode($type) {
    $viewMode = '';
    switch ($type) {
      case 'node':
        $viewMode = 'default';
        break;

      case 'block':
        $viewMode = 'full';
        break;

    }
    return $viewMode;
  }

  /**
   * Prepare the entity for rendering.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   An entity object.
   * @param string $type
   *   The entity type.
   * @param string $extra
   *   Additional view options.
   *
   * @return array
   *   A renderable array for the entity.
   */
  private function viewEntity(EntityInterface $entity, $type, $extra) {

    switch ($type) {
      case 'view':
        $display = 'default';
        if (!empty($extra)) {
          $display = $extra;
        }
        $build = views_embed_view($entity->id(), $display);
        break;

      default:
        $viewBuilder = $this->entityTypeManager->getViewBuilder($type);
        $viewMode = $this->getViewMode($type);
        if (!empty($extra)) {
          $viewMode = $extra;
        }
        $build = $viewBuilder->view($entity, $viewMode);
        break;
    }

    return $build;
  }

}
