This module provides URL paths to display content as a standalone page.

This can be useful in situations such as personalisation,
where content needs to be swapped out.

The module does not handle any of the personalisation.
It only provides the URL for the content.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/standalone

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

USAGE
-----
The following paths are available:

## Nodes
/standalone/node/{id}
/standalone/node/{id}/{view mode}

## Blocks

### Content blocks
/standalone/block_content/{id}

### Plugin blocks
/standalone/block/{id}

## Views
/standalone/view/{id}
/standalone/view/{id}/{display}

## Users
/standalone/user/{id}
