<?php

use Drupal\Core\Entity\EntityInterface;
/**
 * @file
 * Hooks specific to the Standalone Content module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter the render array for content on a standalone page.
 *
 * @param array $build
 *   The render array.
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   The entity.
 * @param string $extra
 *   Extra options.
 * @see \Drupal\standalone\Controller\StandaloneContent::page
 */
function hook_standalone_build_alter(array &$build, EntityInterface $entity, $extra) {
  if ($entity->bundle() == 'node' && $entity->getEntityTypeId() == 'page') {
    $build = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'standalone-page-container',
        ],
      ],
      'extra' => [
        '#type' => 'html_tag',
        '#tag' => 'h1',
        '#value' => t('Extra heading'),
      ],
      'contents' => $build,
    ];
  }
}

/**
 * Alter the rendered markup for content on a standalone page.
 *
 * @param string $markup
 *   The rendered HTML.
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   The entity.
 * @param string $extra
 *   Extra options.
 * @see \Drupal\standalone\Controller\StandaloneContent::page
 */
function hook_standalone_rendered_alter(&$markup, EntityInterface $entity, $extra) {
  $markup .= '<div class="extra-div"></div>';
}

/**
 * @} End of "addtogroup hooks".
 */
